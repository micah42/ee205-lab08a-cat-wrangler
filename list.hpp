///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// SingleLinkedList class
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   11_APR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

using namespace std;

class DoubleLinkedList {
protected:
    Node* head = nullptr;
    Node* tail = nullptr;
    unsigned int count = 0; 

public:
    const bool empty() const;
    void push_front(Node* newNode);
    Node* pop_front();
    Node* get_first() const;
    Node* get_next(const Node* currentNode) const;
    unsigned int size() const; 

    void push_back(Node* newNode);
    Node* pop_back();
    Node* get_last() const;
    Node* get_prev(const Node* currentNode) const;

    bool isIn(Node* aNode) const;
    void insert_after(Node* currentNode, Node* newNode);
    void insert_before(Node* currentNode, Node* newNode);

    bool isBefore(Node* a, Node* b);
    void swap(Node* node1, Node* node2);
    const bool isSorted() const;
    void insertionSort();


};
